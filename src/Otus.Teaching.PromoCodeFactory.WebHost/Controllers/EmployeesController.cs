﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAsync(EmployeeRequest employeeRequest)
        {
            await _employeeRepository.Add(new Employee{ 
               Id = Guid.NewGuid(),
               AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount,
               Email = employeeRequest.Email,
               FirstName = employeeRequest.FirstName,
               LastName = employeeRequest.LastName,
               Roles = new List<Role>()
               {
                    FakeDataFactory.Roles.FirstOrDefault(x => x.Name == "PartnerManager")
                },
            });

            return Ok(true);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var user = await _employeeRepository.GetByIdAsync(id);
            if (user == null)
            {
                return NotFound("Сотрудника с таким id не существует");
            }
            await _employeeRepository.RemoveByIdAsync(id);

            return Ok(true);
        }        
        
        /// <summary>
        /// Редактировать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditAsync(Guid id, EmployeeUpdateRequest employeeUpdateRequest)
        {
            var user = await _employeeRepository.GetByIdAsync(id);
            if (user == null)
            {
                return NotFound("Сотрудника с таким id не существует");
            }

            user.FirstName = employeeUpdateRequest.FirstName;
            user.LastName = employeeUpdateRequest.LastName;
            user.Email = employeeUpdateRequest.Email;
            user.AppliedPromocodesCount = employeeUpdateRequest.AppliedPromocodesCount;


            return Ok(true);
        }

    }
}